## Tools

- Node.js
- Nodemon
- Postgresql
- PgAdmin4

## How to compile

- First "npm install" or "yarn" at root directory
- Execute command "npm run dev" on cli

## Note

- Please change your postgresql database, schema, host, port, username, password at ./src/index.ts