import { isNumber } from "lodash"

export const checkPositiveIntegerNumber = (value : any)=>{
    if(typeof value !== 'undefined' && value !== null && value !== "" && value.trim() !== ""){
        if(isNumber(Number(value)) && Number(value) >= 0 && Number.isInteger(Number(value))){
            return true
        }
    }

    return false
}

export const checkIsNumber = (value : any)=>{
    if(typeof value !== 'undefined' && value !== null && value !== "" && value.trim() !== ""){
        if(isNumber(Number(value))){
            return true
        }
    }

    return false
}