import {
	Entity,
	Column,
	CreateDateColumn,
	UpdateDateColumn,
    PrimaryGeneratedColumn,
	OneToMany,
	ManyToMany,
    ManyToOne,
    JoinColumn,
	JoinTable,
    BaseEntity
} from 'typeorm';

@Entity('home')
export class Home extends BaseEntity {
    @PrimaryGeneratedColumn()
	id: number;

	@Column({
		unique: true
	})
	name: string;

    @Column({
        nullable: true
    })
	desc: string;

    @Column(
		"decimal", { 
			precision: 10,
			scale: 2
		}
	)
	price: number;

	@Column()
	post_code: string;
}