import { createConnection } from 'typeorm';
import express, { Application } from 'express'
import cors from 'cors'
import { Home } from './entity/Home';
import { homeRouter } from './routes/home';
import { postCodeRouter } from './routes/post_code';

const app: Application = express();

const main = async () => {
	try {
        // Database Connection
		await createConnection({
			type: 'postgres',
			host: 'localhost',
			port: 5432,
			username: 'postgres',
			password: "seas0nc04laxx",
			database: 'postgres',
			entities: [Home],
			synchronize: true,
		});
		
		console.log('Connected to Postgres');

        // App Setting
        app.use(cors())
		app.use(express.json());

        // App Router
		app.use(homeRouter);
		app.use(postCodeRouter);

        // App Execute
		app.listen(3001, () => {
			console.log('Now running on port 3001');
		});
	} catch (error) {
		console.error(error);
		throw new Error('Unable to connect to db');
	}
};

main();