import express from 'express';
import { createQueryBuilder } from 'typeorm';
import { Request, Response } from "express"
import { Home } from '../entity/Home';
import { checkStringEmpty } from "../checker/variable_empty"
import moment from 'moment'
import { orderBy } from 'lodash';
const router = express.Router();


// Fetch post_code data
router.get('/postCode', async (req: Request, res: Response) => {
	try{

		// Query post_code data
		const post_code = await createQueryBuilder('home')
			.select(['home.post_code'])
			.from(Home, 'home')
			.getMany();

		// Response success data
		return res.status(200).json({
			code: 200,
			status: "success",
			message: "Query success.",
			payload: post_code,
			count: post_code.length
		})

	}catch(error){
		console.error(`[src/route/pose_code.ts][GET][/postCode][${moment().format('DD/MM/YYYY HH:mm:ss')}] ${error.message}`)
		return res.status(500).json({
			code: 500,
			status: "fail",
			message: error.message,
			payload: [],
			count: 0
		})
	}
});

// Fetch average and median by post_code
router.get('/postCode/:id', async (req: Request, res: Response) => {
	try{

        const { id } = req.params;

        // Check id is not empty
        let check_id:boolean = await checkStringEmpty(id)
        if(!check_id){
        	return res.status(400).json({
        		code: 400,
        		status: "fail",
        		message: "post_code don't be empty"
        	})
        }
        
		// Query price data
		const price = await createQueryBuilder('home')
			.select(['home.price'])
			.from(Home, 'home')
            .where('home.post_code = :post_code', {
                post_code: `${id}`
            })
            .orderBy('home.price', 'ASC')
			.getMany();

        // find average and median by summary of price
        let median:number = 0
        let average:number = 0
        let total:number = 0
        for(let i of price){
            total = total + Number(i.price)
        }

        // fine average
        average = (total/price.length)

        // find median
        if(price.length %2 !== 0){
            median = (Number(price[Math.floor((price.length-1)/2)].price) + Number(price[Math.round((price.length-1)/2)].price)/2)
        }else{
            median = Number(price[(price.length-1)/2])
        }

		// Response success data
		return res.status(200).json({
			code: 200,
			status: "success",
			message: "Query success.",
			payload: {
                average: average,
                median: median
            },
		})

	}catch(error){
		console.error(`[src/route/pose_code.ts][GET][/postCode/:id][${moment().format('DD/MM/YYYY HH:mm:ss')}] ${error.message}`)
		return res.status(500).json({
			code: 500,
			status: "fail",
			message: error.message,
			payload: [],
			count: 0
		})
	}
});

export { router as postCodeRouter };