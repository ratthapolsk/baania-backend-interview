import express from 'express';
import { createQueryBuilder } from 'typeorm';
import { Request, Response } from "express"
import { Home } from '../entity/Home';
import { checkPositiveIntegerNumber, checkIsNumber } from "../checker/check_number"
import moment from 'moment'
const router = express.Router();

// This function separate data by page number and data size when using offset & limit
// const calSkip = (page:number, size:number) => {
//     return (page - 1) * size;
// };

// Fetch home data
router.get('/home', async (req: Request, res: Response) => {
	try{

		// Get skip & take from url params
		let {
			skip,
			take
		} = req.query

		// Check skip & take is number
		let check_skip:boolean = await checkPositiveIntegerNumber(skip)
		let check_take:boolean = await checkPositiveIntegerNumber(take)

		if(!check_skip){
			return res.status(400).json({
				code: 400,
				status: "fail",
				message: "skip is not an integer",
				payload: [],
				count: 0
			})
		}

		if(!check_take){
			return res.status(400).json({
				code: 400,
				status: "fail",
				message: "take is not an integer",
				payload: [],
				count: 0
			})
		}

		// Convert skip & take to number
		let _skip:number = Number(skip)
		let _take:number = Number(take)

		// Query home data by skip & take
		const home = await createQueryBuilder('home')
			.select(['home.id', 'home.name', 'home.desc', 'home.price', 'home.post_code'])
			.from(Home, 'home')
			.skip(_skip)
			.take(_take)
			// This function separate data by page number and data size
			// .offset(calSkip(_skip, _take))
			// .limit(_take)
			.getMany();

		// Response success data
		return res.status(200).json({
			code: 200,
			status: "success",
			message: "Query success.",
			payload: home,
			count: home.length
		})

	}catch(error){
		console.error(`[src/route/home.ts][GET][/home][${moment().format('DD/MM/YYYY HH:mm:ss')}] ${error.message}`)
		return res.status(500).json({
			code: 500,
			status: "fail",
			message: error.message,
			payload: [],
			count: 0
		})
	}
});

// Insert home data
router.post('/home', async (req, res) => {
	try{

		// Get params from request body
		const {
			name,
			desc,
			price,
			post_code,
		} = req.body;

		// Check price is a number
		let check_price:boolean = await checkIsNumber(price)
		if(!check_price){
			return res.status(400).json({
				code: 400,
				status: "fail",
				message: "price is not a number"
			})
		}

		// Insert home data
		const insert_home = Home.create({
			name,
			desc,
			price,
			post_code,
		});

		await insert_home.save();

		// Response success data
		return res.status(200).json({
			code: 200,
			status: "success",
			message: "Insert success."
		})
	}catch(error){
		console.error(`[src/route/home.ts][POST][/home][${moment().format('DD/MM/YYYY HH:mm:ss')}] ${error.message}`)
		return res.status(500).json({
			code: 500,
			status: "fail",
			message: error.message
		})
	}
});

// Update home data
router.patch('/home/:id', async (req, res) => {
	try{

		// Get params from request body
		const {
			name,
			desc,
			price,
			post_code,
		} = req.body;

		// Get home id to update
		const { id } = req.params;

		// Check id is a number
		let check_id:boolean = await checkIsNumber(id)
		if(!check_id){
			return res.status(400).json({
				code: 400,
				status: "fail",
				message: "Id is not a number"
			})
		}

		// Check price is a number
		let check_price:boolean = await checkIsNumber(price)
		if(!check_price){
			return res.status(400).json({
				code: 400,
				status: "fail",
				message: "price is not a number"
			})
		}

		let _id:number = Number(id)

		// Insert home data
		await Home.update({
			id: _id
		},{	name,
			desc,
			price,
			post_code
		});

		// Response success data
		return res.status(200).json({
			code: 200,
			status: "success",
			message: "Update success."
		})
	}catch(error){
		console.error(`[src/route/home.ts][PUT][/home/:id][${moment().format('DD/MM/YYYY HH:mm:ss')}] ${error.message}`)
		return res.status(500).json({
			code: 500,
			status: "fail",
			message: error.message
		})
	}
});

router.delete('/home/:id', async (req, res) => {
		try{
			// Get home id to delete
			const { id } = req.params;

			// Check id is a number
			let check_id:boolean = await checkIsNumber(id)
			if(!check_id){
				return res.status(400).json({
					code: 400,
					status: "fail",
					message: "Id is not a number"
				})
			}

			// Delete home
			await Home.delete(
				Number(id)
			);

			// Response success data
			return res.status(200).json({
				code: 200,
				status: "success",
				message: "Delete success."
			})
		}catch(error){
			console.error(`[src/route/home.ts][DELETE][/home/:id][${moment().format('DD/MM/YYYY HH:mm:ss')}] ${error.message}`)
			return res.status(500).json({
				code: 500,
				status: "fail",
				message: error.message
			})
		}
	}
);

export { router as homeRouter };